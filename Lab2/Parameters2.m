% setup for simulink
k = 0.0502;
J = 0.00000116;
Rm = 10.6;
L = 0.00082;

A = [-k*k/(J*Rm)];
B = [k/(J*Rm) -1/J];
C = [1];
D = [0 0];

% being confused on question 5
SS = ss(A,B,C,D);
TFs = tf(SS);

s = sym('s');
                                                                              
Rms = sym('Rm');
Ls = sym('L');
ks = sym('k');
Js = sym('J');
i1s = sym('i1');

As = [-ks*ks/(Js*Rms)];
Bs = [ks/(Js*Rms) -1/Js];
Cs = [1];
Ds = [0 0];


identity = [1];

Cs*inv(s*identity-As)*Bs+Ds

tfs = C*inv(s*identity-As)*Bs+Ds

% problem 9
K=10;
h=0.01;
t=0.2;
wf=20;

