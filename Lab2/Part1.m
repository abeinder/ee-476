% Vm = [
%     -5;
%     -4.5;
%     -4;
%     -3.5;
%     -3;
%     -2.5;
%     -2;
%     2;
%     2.5;
%     3;
%     3.5;
%     4;
%     4.5;
%     5;
% ];
% 
% Im = [
%    -0.051;
%     -0.051;
%     -0.052;
%     -0.05;
%     -0.047;
%     -.045;
%     -0.045;
%     0.036;
%     0.037;
%     0.04;
%     0.039;
%     0.042;
%     0.041;
%     0.039;
% ];
% 
% Wm = [
%     -83;
%     -72;
%     -63;
%     -53;
%     -43;
%     -34;
%     -25;
%     24;
%     34;
%     44;
%     54;
%     64;
%     73;
%     82;
% ]
% 
% save('backemf_data', "Vm", "Im", "Wm")

load('backemf_data.mat')

bias = -.003;
Im = Im + bias;

% 2: Least squares
A=zeros(size(Vm,1),2);

A(:,1)=-1;
A(:,2)=-Wm;

Y = (Vm-Im*13.1472)./Wm;


% A
% At = A.';
% AtA = At*A;
% AtY = At*Y;
% v = inv(AtA)*AtY
% a1=v(1); a2=v(2);
% 
% x_mesh_points=-100:.01:100;
% hold on
% scatter(Wm,Y,100,'MarkerEdgeColor','k','MarkerFaceColor','c')
% plot(x_mesh_points, -a1-a2*x_mesh_points,'-b')
% title('Back emf vs. Speed')
% ylim([-1,1])
% xlabel('Speed (rad/s)')
% ylabel('Back emf')
% legend('Linear fit')

% 3: Averaging
A=zeros(1,2)

a2s = zeros(size(Vm,1),1);
a1s = zeros(size(Vm,1),1);

for i = 1:size(Wm,1)

A(:,1)=1;
A(:,2)=Wm(i);
A
Y = (Vm(i)-Im(i)*13.1472)./Wm(i)
At = A.'
AtA = At*A
AtY = At*Y
v = AtY./AtA
a1s(i)=v(1); a2s(i)=v(1,2);

end

a1_av = mean(a1s)
a2_av = mean(a2s)

