% setup for simulink
k = 0.0502;
J = 0.00000116;
Rm = 10.6;
L = 0.00082;

A = [-Rm/L -k/L; k/J 0];
B = [1/L 0; 0 -1/J];
C = [0 1];
D = [0 0];

% state space but with an integration
A2 = [-Rm/L 0 -k/L; 0 0 1; k/J 0 0];
B2 = [1/L 0; 0 0; 0 -1/J];
C2 = [0 1 0; 0 0 1];
D2 = [0 0; 0 0;];

% being confused on question 5
SS = ss(A,B,C,D);
TFs = tf(SS);

s = sym('s');

Rms = sym('Rm');
Ls = sym('L');
ks = sym('k');
Js = sym('J');
i1s = sym('i1');

As = [-Rms/Ls -ks/Ls; ks/Js 0];
Bs = [1/Ls 0; 0 -1/Js];
Cs = [0 1];
Ds = [0 0];


identity = [1 0; 0 1];

tfs = Cs*inv(s*identity-As)*Bs+Ds;
inv(s*identity-As);
s*identity-As;

Cs*[s -ks/Ls; -ks/Js s+Rms/Ls]
