clear

% 1:
load("current_data.mat")

bias = -.003;
currents = currents + bias;

% 2: Least squares
% A=zeros(size(speeds,1),3);
% A(:,1)=(speeds.^2);
% A(:,2)=speeds;
% A(:,3)=speeds./abs(speeds);
% A
% At = A.';
% AtA = At*A;
% AtY = At*currents;
% v = inv(AtA)*AtY
% 
% I2=v(1); I1=v(2); I0=v(3);
% scatter(speeds,currents,100,'MarkerEdgeColor','k','MarkerFaceColor','c')
% x_mesh_points=-80:.01:80;
% hold on
% plot(x_mesh_points, I2*x_mesh_points.^2+I1*x_mesh_points+I0*(x_mesh_points./abs(x_mesh_points)),'-b')
% title('Current vs. Speed and Parabolic fit')
% xlabel('Speed')
% ylabel('Current')
% legend('Current vs. Speed', 'Parabolic fit')
% ylim([-.15,.15])

% question 3
% A=zeros(size(speeds,1),1);
% A(:,1)=(speeds);
% A
% At = A.';
% AtA = At*A;
% AtY = At*currents;
% v = inv(AtA)*AtY
% I2=v(1);
% x_mesh_points=-80:.01:80;
% hold on
% scatter(speeds,currents,100,'MarkerEdgeColor','k','MarkerFaceColor','c')
% plot(x_mesh_points, I2*x_mesh_points,'-b')
% title('Current vs. Speed and Viscous fit')
% xlabel('Speed')
% ylabel('Current')
% legend('Current vs. Speed', 'Viscous friction only')
% ylim([-.15,.15])

% question 4
% A=zeros(size(speeds,1),1);
% A(:,1)=(speeds./abs(speeds));
% A
% At = A.';
% AtA = At*A;
% AtY = At*currents;
% v = inv(AtA)*AtY
% I0=v(1);
% x_mesh_points=-80:.01:80;
% hold on
% scatter(speeds,currents,100,'MarkerEdgeColor','k','MarkerFaceColor','c')
% plot(x_mesh_points, I0*x_mesh_points./abs(x_mesh_points),'-b')
% title('Current vs. Speed and Sliding fit')
% xlabel('Speed')
% ylabel('Current')
% legend('Current vs. Speed', 'Sliding friction only')
% ylim([-.15,.15])

% Question 5

% % question 2 version
% A=zeros(size(speeds,1),3);
% A(:,1)=(speeds.^2);
% A(:,2)=speeds;
% A(:,3)=speeds./abs(speeds);
% A
% At = A.';
% AtA = At*A;
% AtY = At*currents;
% v = inv(AtA)*AtY
% I2=v(1); I1=v(2); I0=v(3);
% 
% % question 3 version
% A3=zeros(size(speeds,1),1);
% A3(:,1)=(speeds);
% A3
% A3t = A3.';
% A3tA = A3t*A3;
% A3tY = A3t*currents;
% v = inv(A3tA)*A3tY
% I23=v(1);
% 
% % question 4 version
% A4=zeros(size(speeds,1),1);
% A4(:,1)=(speeds./abs(speeds));
% A4
% A4t = A4.';
% A4tA = A4t*A4;
% A4tY = A4t*currents;
% v = inv(A4tA)*A4tY
% I04=v(1);
% 
% 
% x_mesh_points=-100:.01:100;
% hold on
% scatter(speeds,currents,100,'MarkerEdgeColor','k','MarkerFaceColor','c')
% % question 2 version
% plot(x_mesh_points, I2*x_mesh_points.^2+I1*x_mesh_points+I0*(x_mesh_points./abs(x_mesh_points)),'-m')
% % question 3 version
% plot(x_mesh_points, I23*x_mesh_points,'-r')
% % question 4 version
% plot(x_mesh_points, I04*x_mesh_points./abs(x_mesh_points),'-b')
% 
% title('Current vs. Speed and various fits')
% xlabel('Speed')
% ylabel('Current')
% legend('Current vs. Speed', 'Parabolic fit', 'Viscous friction only', 'Sliding friction only')
% ylim([-.15,.15])


% 5... proposed changes
A=zeros(size(speeds,1),4);
A(:,1)=(speeds.^2).*(speeds./abs(speeds));
A(:,2)=speeds;
A(:,3)=speeds./abs(speeds);
A(:,4)=ones(1,size(speeds,1))
A
At = A.';
AtA = At*A;
AtY = At*currents;
v = inv(AtA)*AtY

I2=v(1); I1=v(2); I0=v(3); I3=v(4);
scatter(speeds,currents,100,'MarkerEdgeColor','k','MarkerFaceColor','c')
x_mesh_points=-80:.01:80;
hold on
plot(x_mesh_points, I2*(x_mesh_points.^2).*(x_mesh_points./abs(x_mesh_points))+I1*x_mesh_points+I0*(x_mesh_points./abs(x_mesh_points))+I3,'-b')
title('Current vs. Speed and New fit')
xlabel('Speed')
ylabel('Current')
legend('Current vs. Speed', 'New fit')
ylim([-.15,.15])