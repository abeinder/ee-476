load('resistance_data.mat')

bias = -.003;
currents = currents + bias;

% 3: Least squares
% A=zeros(size(voltages,1),2);
% A(:,1)=currents;
% A(:,2)=ones(size(voltages,1),1);
% A
% v=pinv(A)*voltages
% a1=v(1); a0=v(2);
% a1 % resistance is slope
% 
% x_mesh_points=-.5:.01:.5;
% hold on
% scatter(currents,voltages,100,'MarkerEdgeColor','k','MarkerFaceColor','c')
% plot(x_mesh_points, a1*x_mesh_points+a0,'-b')
% title('Voltage vs. Current')
% xlabel('Current (A)')
% ylabel('Voltage')
% legend('Linear fit')

% 4: Other method
resistances = voltages./currents;
average_resistance = mean(resistances);

first = voltages - currents*13.1472 - 0.0892;
first_mean = mean(first)

second = voltages - currents*average_resistance;
second_mean = mean(second)
% hold on
% scatter(currents, first)
% scatter(currents, second)
% title('Excess Voltage vs. Current')
% legend('Excess voltage 1st method', 'Excess voltage 2nd method')
% xlabel('Current (A)')
% ylabel('Motor Voltage - Motor Current * Resistance Estimates (V)')
% ylim([-1,1])

% 5:

hold off
scatter(currents, resistances)
title('Resistance vs. Current')
xlabel('Current (A)')
ylabel('Resistance (Vm/Im aka Ohms)')
legend('resistances')
ylim([0,20])
